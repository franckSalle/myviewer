# myViewer
This programm enables to explore with web navigator <http://localhost:5000> a shared folder containing folders of PDF files.
To be compatible linux/windows, don't use space in folders and files name.

## Installation
    apt update
    apt upgrade -y
    apt install git python-pip cifs-utils -y
    pip install flask
    cd ~
    git clone https://gitlab.com/franckSalle/myviewer.git
    #Mount shared folder <sharedFolder> to explore with myViewer in ~/myviewer/partage
    #With <myID>, <myPass> and <myDomain>
    cd ~/myviewer
    mkdir partage
    cp /etc/fstab /etc/fstab_old
    echo "<sharedFolder> ~/myviewer/partage cifs username=<myID>,password=<myPass>,domain=<myDomain>,iocharset=utf8 0 0" >> /etc/fstab
    mount -a

## Launch
    python interface.py

