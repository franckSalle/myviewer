# interface.py
# coding: utf-8

from config import *

import os
import shutil

from flask import Flask
from flask import render_template
from flask import url_for
from flask import request

""" Init web server flask """
app = Flask(__name__)


@app.route('/')
def index():
    listeFichiers = os.listdir(PARTAGE)
    return render_template('index.html', listeFichiers=listeFichiers, dossier='menu', SERVEUR=SERVEUR)

@app.route('/afficher/<dossier>')
def afficher(dossier):
    chemin = PARTAGE + '/' + dossier
    listeFichiers = os.listdir(chemin)
    return render_template('index.html', listeFichiers=listeFichiers, dossier=dossier, SERVEUR=SERVEUR)

@app.route('/lire')
def lire():
    dossier = request.args.get('rep')
    fichier = request.args.get('file')
    source = PARTAGE + '/' + dossier + '/' + fichier
    destination = INSTALL + '/static/tmp/' + fichier
    shutil.copyfile(source, destination)
    return render_template('lire.html', fichier=fichier) 


""" Programm """
if __name__ == '__main__':

    app.run(host='0.0.0.0', debug=False)